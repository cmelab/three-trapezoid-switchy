import signac


def main():
    project = signac.init_project("Jigsaw-Signac-3Traps")
    for kT in [0.2, 0.5, 1, 2, 5, 10]:
        for trap_type in [
            ("Gamma1","Gamma2","Gamma3"),
            ("Delta1","Delta2","Delta3"),
            ("Zeta1","Zeta2","Zeta3"),
            ("Kappa1","Kappa2","Kappa3"),
            ("Lamda1","Lamda2","Lamda3"),
            ("Omicron1","Omicron2","Omicron3"),
        ]:
            statepoint = {"kT": kT}
            statepoint["sticky"] = True
            statepoint["epsilon"] = 50
            statepoint["mix_kT"] = 50
            statepoint["trap_type"] = trap_type
            job = project.open_job(statepoint)
            job.init()


if __name__ == "__main__":
    main()
