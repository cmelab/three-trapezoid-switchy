#!/usr/bin/env python
# coding: utf-8

# In[16]:


from glob import glob
from datetime import timedelta
logfiles = glob("../logfiles/*")

times = []

for file in logfiles:
    with open(file, 'r') as searchfile:
        for line in searchfile:
            if 'ETA 00:00:00' in line:
                if (line.split()[4]==line.split()[6]):
                    times.append(line[5:13])


# In[17]:


from datetime import timedelta

def to_td(times):
    ho, mi, se = times.split(':')
    return timedelta(hours=int(ho), minutes=int(mi), seconds=int(se))

print(str(sum(map(to_td, times), timedelta())))

