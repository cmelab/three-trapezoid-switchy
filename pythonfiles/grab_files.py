import signac

proj = signac.get_project()
filters = {'kT': 10}
proj.find_jobs(filters).export_to('transfer_jobs', path=False)
