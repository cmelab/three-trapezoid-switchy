#!/usr/bin/env sh

rsync -rav --ignore-existing --progress rachelsingleton@fry.boisestate.edu:/home/rachelsingleton/team-switchy-code/pythonfiles/slurm_outputs/* pythonfiles/slurm_outputs
